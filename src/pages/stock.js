import React, { useEffect, useState } from 'react'
import api from '@/api'
import Stock from '@/components/elements/Stock'
import Layout from '@/components/layouts/Layout'

export default function StockList () {
  const [stockProduct, setStockProduct] = useState([])

  const fetchStockList = async() => {
    try {
      const response = await api.get("products")
      setStockProduct(response.data.payload)
    } catch (err) {
      throw Error(err)
    }
  }

  useEffect(() => {
    fetchStockList()
  }, [])
  
  return (
    <Layout>
        <h1>Stock Product</h1>
        <Stock stockProduct={stockProduct}/>
    </Layout>
  )
}
