import React, { useEffect, useState } from 'react';
import Layout from '@/components/layouts/Layout';
import api from '@/api';
import Transactions from '@/components/elements/Transactions';

export default function Transaction(){
  const [transactions, setTransactions] = useState([])

  const fetchTransactionsHistory = async () => {
    try {
      const response = await api.get('transactions')
      const data = await response.data.payload.transactions;      
      setTransactions(data)
    } catch (err) {
      throw Error(err)
    }

  }

  useEffect(() => {
    fetchTransactionsHistory();
  }, [])

  return (
    <Layout>
        <h1>Transactions History</h1>
        <Transactions transactions={transactions}/>
    </Layout>
  )
}
