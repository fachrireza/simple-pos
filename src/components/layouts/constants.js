export const MENU_LIST = [
  {
    name: 'Sales',
    path: '/',
  },
  {
    name: 'Transactions',
    path: '/transaction',
  },
  {
    name: 'Stock',
    path: '/stock',
  },
];
