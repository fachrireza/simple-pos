import React from 'react'
import styles from './ProductList.module.css'
import Image from 'next/image'
import { useCartDispatch } from '@/context/CartContext'

const ProductList = ({products}) => {
    const dispatch = useCartDispatch()

    const handleAddToCart = (product) => {
        dispatch({
            type: 'add',
            payload: product
        }); 
    }

    return (
        <div className={styles['product-list']}>
            {
                products.map((product, index) => {
                    return (
                        <div className={styles['product-list__product-card']} key={index}>
                            <div className={styles['product-list__product-card__image']}>
                                <Image 
                                    src={product.img_product} 
                                    alt={product.name} 
                                    fill
                                    style={{ objectFit: 'contain'}}
                                />
                            </div>
                            <div className={styles['product-list__product-card__desc']}>
                                <p title={product.name}>{product.name}</p>
                                <span title={product.price}>{product.price.toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</span>
                            </div>
                            <div className={styles['product-list__product-card__action']}>
                                <button onClick={() => handleAddToCart(product)}>+</button>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default ProductList
