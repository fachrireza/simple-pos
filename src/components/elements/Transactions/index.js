import React from 'react'
import styles from './Transactions.module.css'

const Transactions = ({transactions}) => {
    return (
        <div className={styles['transactions-list']}>
            {
                transactions.sort((a, b) => b.transaction_date.localeCompare(a.transaction_date)).map((list, index) => {
                    return (
                        <div key={index} className={styles['transactions-list__card']}>
                            <h3>Transactions Number : {list.no_order} </h3>
                            <p>Transactions Date : {list.transaction_date}</p>
                            <p>Product Item :</p>
                            <div className={styles['transactions-list__product-item']}>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            list.products.map((item, itemIndex) => {
                                                return (
                                                    <tr key={itemIndex}>
                                                        <td>{item.product}</td>
                                                        <td>{(item.price).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</td>
                                                        <td>{item.quantity}</td>
                                                        <td>{(item.price * item.quantity).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div className={styles['transactions-list__card-pay']}> 
                                <p>Total Price : {(list.total_price).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</p>
                                <p>Total Paid  : {(list.paid_amount).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</p>
                                <p>Change : {(+list.paid_amount - +list.total_price).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</p>
                            </div>
                        </div>
                    )
                }).sort((a, b) => b.transaction_date - a.transaction_date)
            }
        </div>
    )
}

export default Transactions