import React, { useRef, useState } from 'react'
import Image from 'next/image'
import styles from './Cart.module.css'
import { useCart, useCartDispatch } from '@/context/CartContext'
import { onlyNumber } from '@/helper/utils'
import api from '@/api'

const Cart = () => {
    const ref = useRef()
    const carts = useCart()
    const dispatch = useCartDispatch()
    const [paidAmount, setPaidAmount] = useState()

    const handleAddQuantity = (cart) => {
        dispatch({
            type: 'add',
            payload: cart
        });
    }

    const handleDecreaseQuantity = (cart) => {
        dispatch({
            type: 'decrease',
            payload: cart
        });
    }

    const getTotalPrice = () => {
        let totalPrice = 0;
        for (let i = 0; i < carts.length; i++) {
            totalPrice += carts[i].price * carts[i].quantity
        }

        return totalPrice
    }

    const handleChangePaidAmount = (event) => {
        const { target } = event;
        const { value } = target
        setPaidAmount(value.toLocaleString('en-US', { style: 'currency', currency: 'IDR' }))
    }

    const handleCheckout = async() => {
        try {
            const payload = {
                total_price: +getTotalPrice(),
                paid_amount: +paidAmount,
                products: carts.map(({id, quantity}) => ({id, quantity}))
            }
            console.log(payload);
            const response = await api.post('/transactions', payload)
            setPaidAmount()
            dispatch({
                type: 'clear'
            });
            ref.current.value = ""
            console.log(response);
        } catch (err) {
            console.log(err);
        }
    }

    const isDisableButton = () => {
        return !paidAmount || +paidAmount < +getTotalPrice() || carts.length === 0
    }

    return (
        <div className={styles.cart}>
            <h3>Cart</h3>
            <div className={styles['cart__cart-list']}>
                {
                    carts.map((carts, index) => {
                        return (
                            <div key={index} className={styles['cart-item']}>
                                <div className={styles['cart-item__image']}>
                                    <Image
                                        src={carts.img_product}
                                        alt={carts.name}
                                        fill
                                        style={{objectFit:'contain'}}
                                    />
                                </div>
                                <div className={styles['cart-item__desc']}>
                                    <p>{carts.name}</p>
                                    <p>{carts.price.toLocaleString('en-US', {
                                        style: 'currency',
                                        currency: 'IDR',
                                    })}</p>
                                </div>
                                <div className={styles['cart-item__action']}>
                                    <button onClick={() => handleDecreaseQuantity(carts)}>-</button>
                                    <p>{carts.quantity}</p>
                                    <button onClick={() => handleAddQuantity(carts)}>+</button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            <div className={styles['cart__checkout']}>
                <div className={styles['cart__total-price']}>
                    <p>Total Price :</p>
                    <p>{getTotalPrice()}</p>
                </div>
                <div className={styles['cart__total-pay']}>
                    <label>Paid amount </label> :
                    <input placeholder=' ' ref={ref} onChange={(event) => handleChangePaidAmount(event)} onKeyDown={(event) => onlyNumber(event)}/>
                </div>
                <button onClick={() => handleCheckout()} disabled={isDisableButton()}>Checkout</button>                
            </div>
        </div>
    )
}

export default Cart