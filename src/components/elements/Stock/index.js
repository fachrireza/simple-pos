import React from 'react'
import styles from './Stock.module.css'

const Stock = ({stockProduct}) => {
    return (
        <div className={styles["stock-list"]}>
            <button>Add Product</button>
            <div className={styles['stock-list__product-item']}>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Stock ( pcs )</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            stockProduct.map((stock, itemIndex) => {
                                return (
                                    <tr key={itemIndex}>
                                        <td>{stock.id}</td>
                                        <td>{stock.name}</td>
                                        <td>{(stock.price).toLocaleString('en-US', { style: 'currency', currency: 'IDR' })}</td>
                                        <td>{stock.stock}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Stock