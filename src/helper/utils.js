export const onlyNumber = (e) => {
    const allowed = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57]
    if (allowed.indexOf(e.which) == -1) {
        e.preventDefault();
    }
    return false
}